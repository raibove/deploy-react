# Deploy React

### Description
This GitLab CI/CD pipeline is designed specifically for React applications. It automates the build and deployment process, allowing users to preview their React web applications for every Merge Request using GitLab Pages. The pipeline is configured to support React projects built with tools like Vite or Create React App (CRA).

### Prerequisites
- GitLab repository with code to be deployed pushed to gitlab.
- For Vite projects:
  - Update the Vite configuration (`vite.config.js`) to include `base: process.env.HOMEPAGE` to ensure correct routing.

  ```
    import { defineConfig } from 'vite'
    import react from '@vitejs/plugin-react'

    // https://vitejs.dev/config/
    export default defineConfig({
        plugins: [react()],
        base: process.env.HOMEPAGE
    })
  ```

### How to Run
1. **Configuration**:
   - Modify the `.gitlab-ci.yml` file in your project repository:
    ```
    include:
        - component: gitlab.com/raibove/deploy-react/deploy_react@1.0.1
    ```
     - Pass the `BASE_FOLDER` input to specify the base folder of your React project.
     - Pass the `BUILD_FOLDER` input to specify the build folder (`dist` for Vite, `build` for CRA).
  
2. **Pipeline Execution**:
   - The pipeline consists of two stages: `build` and `deploy`.
   - The `build` stage compiles the React project.
   - The `deploy` stage deploys the built artifacts to GitLab Pages.

3. **Running the Pipeline**:
   - Push your changes to the configured branch.
   - The GitLab CI/CD pipeline will automatically trigger based on your GitLab CI/CD settings and the defined rules in the `.gitlab-ci.yml` file.

### Contribution
Contributions to improve and enhance this GitLab CI/CD pipeline are welcome! We aim to extend support for other frameworks and optimize the pipeline further in the future. If you have any suggestions, feature requests, or bug reports, please feel free to:
- Open an issue in the project repository to discuss changes or enhancements.
- Fork the repository, make your changes, and submit a pull request for review.

Your contributions can help make this CI/CD pipeline more versatile and efficient for React developers.

Thank you for your interest and support in improving our CI/CD practices!

---

The reason this project is created is to allow users to preview their web application for every branch using GitLab Pages. In the future, we plan to add support for other frameworks and optimize the pipeline further to accommodate a wider range of development needs.
